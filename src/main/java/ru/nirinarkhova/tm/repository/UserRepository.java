package ru.nirinarkhova.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.nirinarkhova.tm.api.repository.IUserRepository;
import ru.nirinarkhova.tm.model.User;

import java.util.Optional;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    @Override
    public Optional<User> findByLogin(@NotNull final String login) {
        return entities.stream()
                .filter(user -> login.equals(user.getLogin()))
                .findFirst();
    }

    @NotNull
    @Override
    public Optional<User> findByEmail(@NotNull final String email) {
        return entities.stream()
                .filter(user -> email.equals(user.getEmail()))
                .findFirst();
    }

    @Override
    public void removeByLogin(@NotNull final String login) {
        @NotNull final Optional<User> user = findByLogin(login);
        user.ifPresent(this::remove);
    }

}
