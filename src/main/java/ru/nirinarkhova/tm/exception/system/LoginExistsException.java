package ru.nirinarkhova.tm.exception.system;

import ru.nirinarkhova.tm.exception.AbstractException;

public class LoginExistsException extends AbstractException {

    public LoginExistsException() {
        super("Error! Login already exists...");
    }

}
