package ru.nirinarkhova.tm.api.service;

import org.jetbrains.annotations.Nullable;

public interface ServiceLocator {

        @Nullable
        ITaskService getTaskService();

        @Nullable
        IProjectService getProjectService();

        @Nullable
        ICommandService getCommandService();

        @Nullable
        IProjectTaskService getProjectTaskService();

        @Nullable
        IUserService getUserService();

        @Nullable
        IAuthService getAuthService();

}